deploy a machine from Fedora CoreOS iso (after booting into it):
```
sudo coreos-installer install /dev/sda \
    --ignition-url https://gitlab.gnugen.ch/gnugen/infrastructure/new-super-infra-base/-/raw/main/coreos/demo.ign
```

generate ignition file [.ign] (json) from butane file [.bu] (yml):

```
butane --pretty --strict example.bu > example.ign
```