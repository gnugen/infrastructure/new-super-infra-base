variant: fcos
version: 1.4.0

passwd:
  users:
    - name: core
      ssh_authorized_keys:
        - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDSbYV+6FqX3kj9Oll0MRBoYhYBmn8X1dKy8nV5+IcdDctBHgvKmYqdeRvkncWTNq8iuaN7jdGFYODAJXiZRteJqGHkehmZ6dxvZVOJLSUXeOyRQxRjkjI7F9V3cEBXzQX+fh6q/Vey5UwCz8cgLCp23IXsFl7Rl+u3K3L+akoAQIDerH17ggtyYBiDPhyiLxESCOnrMoacIMVTM9tePZJhB6i4Rkgi8h/YwM7qokeWI58hzm71xhkxbxSGe7CH5NTHpX4cd1o0UsYjMkTqQAN5GCrEn7HtjMXpDF8rK3azTu9MddusVFM5CC0o8mmWB2RdIJb8LvUlaRX/apFsD9fFgrWGmizubvUoPAHfqHZYLSKWicg5NMpv6hKnh3V+WZSOU9Boh3VSgrC/r/VNTkBQdq85xykWfzw5h2PYZ1y+OtfNNP2eqprjNEOL6v/nOj5/unCC8UM4MNSFA3Hm2+RsJtBIgUqx3+s8buWmwmYbHsdhogcR2rr0S9ceSXPYlv7Zk1B+R/Xo+br45kgZbfEkMAj7T2Ca1zx7g2pRo696XwZQBi3FyQEeE4N7GSW3GOPWWOOcDzFgTjED4+Au0FpH3D2/8W0DT0EVlIxvdMlB7komAL+CQPhBYjwfVWnTa0V29eG2MQIVu4paeAaNptMDF7hZLUJOLO2ywc+mwAIQzQ== openpgp:0x42C972AA jonas@violoncello.ch


storage:
  files:
    - path: /etc/NetworkManager/system-connections/ens18.nmconnection
      mode: 0600
      contents:
        inline: |
          [connection]
          id=ens18
          type=ethernet
          interface-name=ens18
          [ipv4]
          address1=192.26.40.47/25,192.26.40.1
          dns=128.178.15.7;128.178.15.8;
          may-fail=false
          method=manual

    - path: /etc/hostname
      mode: 0644
      contents:
        inline: staging

    - path: /etc/vconsole.conf
      mode: 0644
      contents:
        inline: KEYMAP=ch

systemd:
  units:
    # Installing podman-compose as a layered package with rpm-ostree
    - name: rpm-ostree-install-podman-compose.service
      enabled: true
      contents: |
        [Unit]
        Description=Layer podman-compose with rpm-ostree
        Wants=network-online.target
        After=network-online.target
        # We run before `zincati.service` to avoid conflicting rpm-ostree
        # transactions.
        Before=zincati.service
        ConditionPathExists=!/var/lib/%N.stamp

        [Service]
        Type=oneshot
        RemainAfterExit=yes
        # `--allow-inactive` ensures that rpm-ostree does not return an error
        # if the package is already installed. This is useful if the package is
        # added to the root image in a future Fedora CoreOS release as it will
        # prevent the service from failing.
        ExecStart=/usr/bin/rpm-ostree install --apply-live --allow-inactive podman-compose
        ExecStart=/bin/touch /var/lib/%N.stamp

        [Install]
        WantedBy=multi-user.target