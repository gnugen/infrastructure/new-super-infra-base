# Mail setup


## Config
comprehensive documentation is available under https://docker-mailserver.github.io/docker-mailserver/latest/

> make sure ports are open on firewall

### DNS
MX, A as well as PTR records need to be set up correctly

PTR record is reverse DNS (IP to Domain resolution); as the Network is managed by EPFL, we can't influence this, so we use the hostname gnusrv[*].epfl.ch as domain for the Mailserver

Additionally we need records for DKIM, DMARC & SPF

### Rspamd
used, as this will become the default in the future anyways as announced (as of october 2023)
`ENABLE_RSPAMD=1`, disable opendkim, opendmarc etc (as in mailserver.env)


### TLS
certif requested by traefik -> whoami service with traefik labels to get certif for domain
some `SSL_*` defined in mailserver.env so that
docker-mailserver searches certificate for domain in acme.json

### SPF


### DKIM
to generate dkim for the domain gnugen.ch: `docker exec -ti mailserver setup config dkim domain gnugen.ch`  
:warning: when adding a second domain, so while the file `storage/config/rspamd/override.d/dkim_signing.conf` already exists, the config is not adapted.
Move the existing config first, then run the setup command and finally merge the two conf files.

(as of oct 2023, using a subdomain for dkim doesn't work)

add the DKIM Record as provided in `storage/config/rspamd/dkim/<keytype>-<selector>-<domain>.public.txt` to the DNS


### DMARC



### MAIL sending to @epfl.ch MX
MX for epfl.ch is wrong when asking internal epfl DNS  
it replies with `smtp[0,4,5].epfl.ch` while from the interent the MX response is `mx[1-3].epfl.ch` 
in order to deliver mail to @epfl.ch we need to instruct the mail agent to send mails to mx1.epfl.ch  
to do this we configure the following fields in `postfix-main.cf`:
`relay_domains`, `transport_maps`, `relay_recipient_maps`  
docker-mailserver merges `postfix-main.cf` into the postfix main config
